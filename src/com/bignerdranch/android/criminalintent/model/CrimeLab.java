package com.bignerdranch.android.criminalintent.model;


import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.google.inject.Inject;
import roboguice.inject.ContextSingleton;

import javax.inject.Provider;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Singleton
public class CrimeLab {
    private List<Crime> mCrimes;

    private static final String TAG = "CrimeLab";
    private static final String FILENAME = "crimes.json";

    private CriminalIntentJSONSerializer mSerializer;




    @Inject
    Application application;

    @Inject
    public CrimeLab(Provider<Context> context) {
        mSerializer = new CriminalIntentJSONSerializer(context.get(), "dataFile");

        try {
            mCrimes = mSerializer.loadCrimes();
        } catch (Exception e) {
            mCrimes = new ArrayList<Crime>();
            Log.e(TAG, "Error loading crimes: ", e);
        }

       /* for (int i = 0; i < 100; i++) {
            Crime c = new Crime();
            c.setTitle("Crime #" + i);
            c.setSolved(i % 2 == 0); // Every other one
            mCrimes.add(c);
        }*/
    }

    public void addCrime(Crime c) {
        mCrimes.add(c);
    }


    public boolean saveCrimes() {
        try {
            mSerializer.saveCrimes(mCrimes);
            Log.d(TAG, "crimes saved to file");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving crimes: ", e);
            return false;
        }
    }

    public List<Crime> getCrimes() {
        return mCrimes;
    }

    public Crime getCrime(UUID id) {
        for (Crime c : mCrimes) {
            if (c.getId().equals(id))
                return c;
        }
        return null;
    }
}
