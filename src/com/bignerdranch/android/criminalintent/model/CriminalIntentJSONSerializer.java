package com.bignerdranch.android.criminalintent.model;


import android.content.Context;
import android.os.Environment;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CriminalIntentJSONSerializer {
    private Context mContext;
    private String mFilename;

    boolean mExternalStorageAvailable = false;
    boolean mExternalStorageWriteable = false;


    public CriminalIntentJSONSerializer(Context c, String f) {
        mContext = c;
        mFilename = f;
    }

    public boolean isExtStorageReadyToRead() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    public boolean isExtStorageReadyToWrite() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
        /*if (Environment.MEDIA_MOUNTED.equals(state)) {
            mExternalStorageAvailable = mExternalStorageWriteable = true;
            Log.d("CriminalIntentJSONSerializer", "yes, it is available and writable");
        }
        else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
            Log.d("CriminalIntentJSONSerializer", "yes, it is available read-only");
        }
        else {
            mExternalStorageAvailable = mExternalStorageWriteable = false;
            Log.d("CriminalIntentJSONSerializer", "no, it is not available");
        }*/
    }

    public boolean saveCrimesToExtStorage(List<Crime> crimes) throws JSONException, IOException {
        if(! isExtStorageReadyToWrite()) {
            Log.d("CriminalIntentJSONSerializer", "not ready for write to ext storage");
            return false;
        }

        JSONArray array = new JSONArray();
        for (Crime c : crimes)
            array.put(c.toJSON());
        // Write the file to disk
        if (mContext == null) {
            Log.d("CriminalIntentJSONSerializer", "mContext is null");
        }
        Writer writer = null;
        try {
            File file = new File(mContext.getExternalFilesDir(null), mFilename);

            OutputStream out = new FileOutputStream(file);

            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if (writer != null)
                writer.close();
        }

        return true;
    }

    public void saveCrimes(List<Crime> crimes)
            throws JSONException, IOException {
        // Build an array in JSON
        JSONArray array = new JSONArray();
        for (Crime c : crimes)
            array.put(c.toJSON());
        // Write the file to disk
        if (mContext == null) {
            Log.d("CriminalIntentJSONSerializer", "mContext is null");
        }
        Writer writer = null;
        try {
            OutputStream out = mContext
                    .openFileOutput(mFilename, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if (writer != null)
                writer.close();
        }
    }

/*    public void saveCrimesToExtStorage(List<Crime> crimes) {
        throws JSONException, IOException
    }*/

    public List<Crime> loadCrimesFromExtStorage() throws IOException, JSONException {
        if(! isExtStorageReadyToRead()) {
            Log.d("CriminalIntentJSONSerializer", "not ready for write to ext storage");
            return Collections.emptyList();
        }

        List<Crime> crimes = new ArrayList<>();
        File file = new File(mContext.getExternalFilesDir(null), mFilename);
        BufferedReader reader = null;

        try {
            InputStream is = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder jsonString = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
// Line breaks are omitted and irrelevant
                jsonString.append(line);
            }
// Parse the JSON using JSONTokener
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
// Build the array of crimes from JSONObjects
            for (int i = 0; i < array.length(); i++) {
                crimes.add(new Crime(array.getJSONObject(i)));
            }
        } catch (FileNotFoundException e) {


        }
        finally {
            if (reader != null) {
                reader.close();
            }
        }
        return crimes;

        // mContext.openFileInput(file);
    }

    public List<Crime> loadCrimes() throws IOException, JSONException {
        List<Crime> crimes = new ArrayList<>();
        BufferedReader reader = null;

        try {
            InputStream in = mContext.openFileInput(mFilename);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
// Line breaks are omitted and irrelevant
                jsonString.append(line);
            }
// Parse the JSON using JSONTokener
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
// Build the array of crimes from JSONObjects
            for (int i = 0; i < array.length(); i++) {
                crimes.add(new Crime(array.getJSONObject(i)));
            }
        } catch (FileNotFoundException e) {
// Ignore this one; it happens when starting fresh
        } finally {
            if (reader != null)
                reader.close();
        }
        return crimes;
    }
    // Open and read the file into a StringBuilder

}
