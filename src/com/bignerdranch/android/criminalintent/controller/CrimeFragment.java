package com.bignerdranch.android.criminalintent.controller;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import com.bignerdranch.android.criminalintent.R;
import com.bignerdranch.android.criminalintent.model.Crime;


import com.bignerdranch.android.criminalintent.model.CrimeLab;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

import javax.inject.Inject;
import java.util.Date;
import java.util.UUID;

public class CrimeFragment extends RoboFragment {

    private Crime mCrime;

    @InjectView(R.id.crime_title)
    private EditText mCrimeTitle;

    @InjectView(R.id.choice_button)
    private Button mChoiceButton;


    @InjectView(R.id.crime_solved)
    private CheckBox mSolvedCheckBox;

    @Inject
    CrimeLab crimeLab;



    private static final String DIALOG_CHOICE = "choice";
    private static final int REQUEST_DATE_TIME = 0;

    public static final String EXTRA_CRIME_ID = "com.bignerdranch.android.criminalintent.controller.crime_id";

    public static CrimeFragment newInstance(UUID crimeId) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CRIME_ID, crimeId);
        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        UUID crimeId = (UUID)getArguments().getSerializable(EXTRA_CRIME_ID);
        mCrime = crimeLab.getCrime(crimeId);
    }

    @Override
    public void onPause() {
        super.onPause();
        crimeLab.saveCrimes();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        updateDateButtonText();

        mChoiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                DialogChoiceFragment dialog = DialogChoiceFragment.newInstance(mCrime.getDate());
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_DATE_TIME);
                dialog.show(fm, DIALOG_CHOICE);
            }
        });


        mSolvedCheckBox.setChecked(mCrime.isSolved());
        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setSolved(isChecked);
            }
        });

        mCrimeTitle.setText(mCrime.getTitle());
        mCrimeTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence userInput, int start, int before, int count) {
                mCrime.setTitle(userInput.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crime, parent, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB &&
                NavUtils.getParentActivityName(getActivity()) != null) {
            getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if ( NavUtils.getParentActivityName(getActivity()) != null ) {
                    NavUtils.navigateUpFromSameTask(getActivity());
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_DATE_TIME) {
            Date date = (Date)data
                    .getSerializableExtra(DialogChoiceFragment.EXTRA_DATE_TIME);
            mCrime.setDate(date);
            updateDateButtonText();
        }

    }

    private void updateDateButtonText() {
        mChoiceButton.setText(mCrime.getDate().toString());
    }

}
