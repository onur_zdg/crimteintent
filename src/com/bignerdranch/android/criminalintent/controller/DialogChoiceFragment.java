package com.bignerdranch.android.criminalintent.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import com.bignerdranch.android.criminalintent.R;
import com.bignerdranch.android.criminalintent.model.CrimeLab;
import roboguice.fragment.RoboDialogFragment;

import javax.inject.Inject;
import java.util.Date;

public class DialogChoiceFragment extends RoboDialogFragment {


    @Inject
    private CrimeLab crimeLab;

    private Button mDateButton;
    private Button mTimeButton;

    private static final String DIALOG_DATE = "date";
    private static final String DIALOG_TIME = "time";


    /**
     * Request params should go with the originator
     */
    private static final int REQUEST_DATE = 1;
    private static final int REQUEST_TIME = 2;

    /**
     * Fragment/Activity params should go with the original Fragment/Activity
     */
    public static final String EXTRA_DATE_TIME = "com.bignerdranch.android.criminalintent.controller.dateTime";

    private Date mDate;


    public static DialogChoiceFragment newInstance(Date date) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DATE_TIME, date);
        DialogChoiceFragment fragment = new DialogChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater()
                .inflate(R.layout.dialog_choice, null);

       mDate = (Date) getArguments().getSerializable(EXTRA_DATE_TIME);

        mDateButton = (Button) view.findViewById(R.id.crime_date);
        mDateButton.setText("Change date");
        mDateButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Date crimeDate = (Date) getArguments().getSerializable(EXTRA_DATE_TIME);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(crimeDate);
                dialog.setTargetFragment(DialogChoiceFragment.this, REQUEST_DATE);
                dialog.show(fm, DIALOG_DATE);
            }
        });


        mTimeButton = (Button) view.findViewById(R.id.crime_time);
        mTimeButton.setText("Change time");
        mTimeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Date crimeDate = (Date) getArguments().getSerializable(EXTRA_DATE_TIME);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                TimePickerFragment dialog = TimePickerFragment.newInstance(crimeDate);
                dialog.setTargetFragment(DialogChoiceFragment.this, REQUEST_TIME);
                dialog.show(fm, DIALOG_TIME);
            }
        });


        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.date_picker_title)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendResult(Activity.RESULT_OK);
                    }
                }).create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_DATE) {
            mDate = (Date)data
                    .getSerializableExtra(DatePickerFragment.EXTRA_DATE);
        }
        else if(requestCode == REQUEST_TIME) {
            mDate = (Date)data
                    .getSerializableExtra(TimePickerFragment.EXTRA_TIME);
        }
        getArguments().putSerializable(EXTRA_DATE_TIME, mDate);

    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() == null) {
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_DATE_TIME, mDate);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }

}

