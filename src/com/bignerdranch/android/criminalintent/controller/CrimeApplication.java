package com.bignerdranch.android.criminalintent.controller;

import android.app.Application;
import roboguice.RoboGuice;

public class CrimeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE,
                RoboGuice.newDefaultRoboModule(this), new ApplicationModule());
    }
}